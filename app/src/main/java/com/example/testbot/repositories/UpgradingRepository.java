package com.example.testbot.repositories;

import com.example.testbot.assets.UpgradingBuilding;
import com.example.testbot.helpers.Regex;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

public class UpgradingRepository {
    private static UpgradingRepository instance;


    public static UpgradingRepository getInstance() {
        if (instance == null) {
            instance = new UpgradingRepository();
        }

        return instance;
    }


    /**
     * Update upgrading buildings list with a newly requested Jsoup {@link Document}
     * @param page Jsoup {@link Document} contains upgrading buildings info
     * @return ArrayList contains a list of upgrading buildings
     */
    public ArrayList<UpgradingBuilding> updateUpgradingList(Document page) {
        ArrayList<UpgradingBuilding> upgradingBuildings = new ArrayList<>();
        try {
            // get the html soup contains upgrading buildings
            Elements buildingsSoup = page.getElementsByClass("boxes buildingList")
                .get(0)
                .getElementsByClass("name");

            int index = 0;
            try {
                // Loop through all currently upgrading buildings
                //noinspection InfiniteLoopStatement
                while (true) {
                    String name = buildingsSoup.get(index).ownText();
                    Element levelSoup = buildingsSoup.get(index).getElementsByClass("lvl").get(0);
                    String level = Regex.findFirst("(?:Level )(\\d.?)", levelSoup.text());
                    String timeLeft = page.getElementsByClass("buildDuration")
                        .get(index)
                        .getElementsByClass("timer")
                        .get(0)
                        .attr("value");

                    upgradingBuildings.add(new
                        UpgradingBuilding(name, Integer.parseInt(level), Integer.parseInt(timeLeft)));

                    index++;
                }
            } catch (IndexOutOfBoundsException ignore) {
                // out of bound after catch the last item in the list
            }
        } catch (IndexOutOfBoundsException ignore) {
            // Out of bounds means no currently upgrading buildings
        } catch (Exception e) {
            e.printStackTrace();
        }

        return upgradingBuildings;
    }
}
