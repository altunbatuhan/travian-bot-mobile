package com.example.testbot.repositories;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.example.testbot.assets.Building;
import com.example.testbot.assets.User;
import com.example.testbot.helpers.Regex;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;

import static com.example.testbot.Constants.VERBOSE_LOG_TAG;

/**
 * This repository handles all crawling actions related to buildings, both inner and outer ones
 */
public class BuildingsRepository {
    private static BuildingsRepository instance;

    private MutableLiveData<ArrayList<Building>> mOuterBuildings = new MutableLiveData<>();

    private MutableLiveData<ArrayList<Building>> mInnerBuildings = new MutableLiveData<>();
    private MutableLiveData<Document> mLatestPage = new MutableLiveData<>();

    public static BuildingsRepository getInstance() {
        if (instance == null) {
            instance = new BuildingsRepository();
        }

        return instance;
    }


    /**
     * Crawl inner building list info and generate a list contains {@link Building} objects
     */
    @SuppressLint("StaticFieldLeak")
    public void updateInnerBuildingList() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                try {
                    Log.v(VERBOSE_LOG_TAG, "Requesting a new dorf2 page contains inner building info");
                    Document doc = Jsoup.connect(User.serverUrl + "dorf2.php")
                        .cookies(User.cookies)
                        .get();

                    Log.v(VERBOSE_LOG_TAG, "A new dorf2 page contains inner building info " +
                        "successfully requested");

                    updateInnerBuildingList(doc);
                    mLatestPage.postValue(doc);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }


    /**
     * Crawl through a newly requested JSoup {@link Document} to get inner buildings info, generate a list
     * contains Building objects
     *
     * @param doc JSoup {@link Document} contains inner buildings info
     */
    public void updateInnerBuildingList(Document doc) {

        // Check if this  document contains inner building info
        if (!doc.location().contains("dorf2"))
            return;

        Log.v(VERBOSE_LOG_TAG, "Scraping a document contains inner Building info");

        ArrayList<Building> buildings = new ArrayList<>();

        //noinspection SpellCheckingInspection
        User.isShowingUpgradeIndicator = doc.toString().contains("lswitchMinus");

        // Get inner building map
        Element map = doc.getElementById("village_map");

        for (Element element : map.children()) {
            try {
                // If this area is an empty building site
                if (element.child(0).attr("class").equals("building iso")) {
                    String urlRaw = element
                        .child(1)
                        .child(0)
                        .child(0)
                        .attr("onclick");
                    String url = Regex.findFirst("(?:window.location.href=')(.+?)(?:')", urlRaw);

                    String name = element.child(1).child(0).text();

                    // Add this empty building site to list
                    buildings.add(new Building(name, url, true));
                } else {
                    // Else this area must already have had a building, so let's scrape it's name & level

                    String nameLevelRaw = element.child(0).attr("title");
                    String name = Regex.findFirst("(.+?)(?: <span)", nameLevelRaw);

                    // Skip this area if cannot find its name
                    if (name == null)
                        continue;

                    // Level info must be scraped in different ways if the user turn on/off
                    // "upgrade indicator" feature
                    String level;
                    if (User.isShowingUpgradeIndicator)
                        level = element.getElementsByClass("labelLayer").text();
                    else
                        level = element.child(0).text();

                    // Scrape building's url
                    String urlRaw = element.child(0).attr("onclick");
                    String url = Regex.findFirst("(?:window.location.href=')(.+?)(?:')", urlRaw);

                    // Add this building to list
                    buildings.add(new Building(name, Integer.parseInt(level), url));
                }
            } catch (IndexOutOfBoundsException ignore) {}
        }

        mInnerBuildings.postValue(buildings);
    }


    /**
     * Crawl outer building list info and generate a list contains {@link Building} objects
     */
    //Memory leak can be omitted since the crawling process won't last very long
    @SuppressLint("StaticFieldLeak")
    public void updateOuterBuildingList() {

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    Log.v(VERBOSE_LOG_TAG, "Requesting a new dorf1 page contains outer building info");

                    // Crawl outer buildings info
                    Document doc = Jsoup.connect(User.serverUrl + "dorf1.php")
                        .cookies(User.cookies)
                        .get();

                    Log.v(VERBOSE_LOG_TAG, "A new dorf1 page contains outer building info " +
                        "successfully requested");

                    updateOuterBuildingList(doc);
                    mLatestPage.postValue(doc);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

        }.execute();
    }


    /**
     * Crawl through a newly requested JSoup {@link Document} to get outer buildings info, generate a list
     * contains Building objects
     *
     * @param document JSoup document contains outer buildings info
     */
    public void updateOuterBuildingList(Document document) {
        // Check if this document contains outer building info
        if (!document.location().contains("dorf1"))
            return;

        Log.v(VERBOSE_LOG_TAG, "Scraping a document contains outer Building info");

        ArrayList<Building> buildings = new ArrayList<>();

        // Find all buildings html tags
        Elements areas = document.select("area");

        // Not all new pages contain outer buildings info, so if there's none, just simply skip
//        if (areas.size() <= 18)
//            return;

        for (Element area : areas) {

            String nameAndLevel = area.attr("alt");
            String url = area.attr("href");

            // Skip last area tag contains dorf2.php
            if (url.contains("dorf2.php"))
                continue;

            String name = Objects.requireNonNull(Regex
                .findFirst("^(.+?)(?: Level)", nameAndLevel));

            int level = Integer.parseInt(Objects.requireNonNull(Regex
                .findFirst("(?:Level )(\\d.?)", nameAndLevel)));

            buildings.add(new Building(name, level, url));
        }

        mOuterBuildings.postValue(buildings);
    }


    public MutableLiveData<ArrayList<Building>> getOuterBuildings() {
        return mOuterBuildings;
    }


    public MutableLiveData<ArrayList<Building>> getInnerBuildings() {
        return mInnerBuildings;
    }


    public MutableLiveData<Document> latestPage() {
        return mLatestPage;
    }
}
