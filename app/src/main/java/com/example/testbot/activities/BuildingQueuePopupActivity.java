package com.example.testbot.activities;

import android.annotation.SuppressLint;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.ItemTouchHelper;
import android.transition.Slide;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import com.example.testbot.Constants.ResultCode;
import com.example.testbot.R;
import com.example.testbot.adapters.QueuingsRecyclerviewAdapter;
import com.example.testbot.adapters.UpgradingsRecyclerViewAdapter;
import com.example.testbot.assets.BuildingQueue;
import com.example.testbot.assets.UpgradingBuilding;
import com.example.testbot.helpers.Listener;
import com.example.testbot.viewmodels.BuildingQueueViewModel;

import java.util.ArrayList;
import java.util.Collections;

public class BuildingQueuePopupActivity extends PopupActivity {
    private BuildingQueueViewModel mViewModel;
    private UpgradingsRecyclerViewAdapter mUpgradingsAdapter;
    private QueuingsRecyclerviewAdapter mQueuingsAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_building_queue);

        ArrayList<UpgradingBuilding> upgradings = getIntent()
                                .getParcelableArrayListExtra("upgradings");

        BuildingQueue queuings = getIntent().getParcelableExtra("queuings");

        initUpgradingsRecyclerView(upgradings);
        initQueuingsRecyclerView(queuings);

        mViewModel = ViewModelProviders.of(this).get(BuildingQueueViewModel.class);
        mViewModel.setUpgradingBuildings(upgradings);
        mViewModel.setQueuingBuildings(queuings);
        mViewModel.initTimer();

        mViewModel.upgradingBuildings().observe(this, new Observer<ArrayList<UpgradingBuilding>>() {
            @Override
            public void onChanged(@Nullable ArrayList<UpgradingBuilding> upgradingBuildings) {
                assert upgradingBuildings != null;
                mUpgradingsAdapter.updateUpgradingList(upgradingBuildings);
                mUpgradingsAdapter.notifyDataSetChanged();

                // Showing empty indicator if the upgrading list was emptied
                if (upgradingBuildings.isEmpty()) {
                    TextView upgradingsEmpty = findViewById(R.id.tv_upgradings_empty);
                    upgradingsEmpty.setVisibility(View.VISIBLE);
                }
            }
        });

        mViewModel.queuingBuildings().observe(this, new Observer<BuildingQueue>() {
            @Override
            public void onChanged(@Nullable BuildingQueue buildings) {
                assert buildings != null;
                mQueuingsAdapter.updateQueue(buildings);
                mQueuingsAdapter.notifyDataSetChanged();

                // Showing empty indicator if the upgrading list was emptied
                if (buildings.isEmpty()) {
                    TextView queuingsEmpty = findViewById(R.id.tv_queuings_empty);
                    queuingsEmpty.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    /**
     * Initializing the recycler view contain upgrading buildings list
     * @param buildings The list contains all upgrading buildings to be displayed
     */
    private void initUpgradingsRecyclerView(ArrayList<UpgradingBuilding> buildings) {
        RecyclerView upgradingsRecyclerView = findViewById(R.id.rv_upgrading);
        mUpgradingsAdapter = new UpgradingsRecyclerViewAdapter(this, buildings);
        upgradingsRecyclerView.setAdapter(mUpgradingsAdapter);
        upgradingsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        upgradingsRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        if (buildings.isEmpty()) {
            TextView upgradingsEmpty = findViewById(R.id.tv_upgradings_empty);
            upgradingsEmpty.setVisibility(View.VISIBLE);
        }


    }


    /**
     * Initializing the recycler view contain building queue, along with its listeners
     * @param buildings The building queue to be displayed
     */
    @SuppressLint("ClickableViewAccessibility")
    private void initQueuingsRecyclerView(BuildingQueue buildings) {
        RecyclerView queuingsRecyclerView = findViewById(R.id.rv_queuing);

        mQueuingsAdapter = new QueuingsRecyclerviewAdapter(this, buildings, new Listener() {
            @Override
            public void onButtonClicked(View v, int index) {
                deleteQueueItem(index);
            }


            @Override
            public void onItemClicked(View v, int position) {

            }
        });

        queuingsRecyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                /*
                 * Re-sort the queue to maintain consistency after finger release and some items
                 * has been swapped
                 */
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (mQueuingsAdapter.itemMoved) {
                        mQueuingsAdapter.buildings.resortAfterSwap();
                        mQueuingsAdapter.notifyDataSetChanged();
                    }
                }
                return false;
            }
        });
        queuingsRecyclerView.setAdapter(mQueuingsAdapter);
        queuingsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        /*
         *  Add drag, drop and swipe listener.
         */
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP|ItemTouchHelper.DOWN, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder start, @NonNull RecyclerView.ViewHolder goal) {
                int startIndex = start.getAdapterPosition();
                int goalIndex  = goal.getAdapterPosition();

                /*
                 * Swap 2 items with each other, notify that the queue's virginity (consistency) has
                 * been defiled so we will have to purify (re-sort) it after the user done moving item
                 */
                Collections.swap(mQueuingsAdapter.buildings, startIndex, goalIndex);
                mQueuingsAdapter.notifyItemMoved(startIndex, goalIndex);
                mQueuingsAdapter.itemMoved = true;
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                int index = viewHolder.getAdapterPosition();
                deleteQueueItem(index);
            }
        }).attachToRecyclerView(queuingsRecyclerView);

        if (buildings.isEmpty())
            findViewById(R.id.tv_queuings_empty).setVisibility(View.VISIBLE);
    }


    @Override
    void initPopupWindow() {
        Slide slide = new Slide();
        slide.setSlideEdge(Gravity.BOTTOM);
        slide.excludeTarget(android.R.id.statusBarBackground, true);
        getWindow().setEnterTransition(slide);

        super.initPopupWindow();
    }


    /**
     * Delete an item from building queue, and all of its higher level references
     * @param index Index of the item to be removed
     */
    private void deleteQueueItem(int index) {
        ArrayList<Integer> removedIndexes = mQueuingsAdapter.buildings.removeBuilding(index);

        // Remove from recycler view with animation
        for (Integer i : removedIndexes)
            mQueuingsAdapter.notifyItemRemoved(i);

        // Showing empty indicator if the upgrading list was emptied
        if (mQueuingsAdapter.buildings.isEmpty())
            findViewById(R.id.tv_queuings_empty).setVisibility(View.VISIBLE);
    }


    @Override
    public void clickedOutside(View view) {
        finishAfterTransition();
    }


    /**
     * Make sure to return modified building queue before closing this activity
     */
    @Override
    public void finishAfterTransition() {
        Intent output = new Intent();
        BuildingQueue queuings = mViewModel.queuingBuildings().getValue();
        output.putExtra("queuings", (Parcelable) queuings);
        setResult(ResultCode.OK, output);
        super.finishAfterTransition();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
