package com.example.testbot.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.example.testbot.R;
import com.example.testbot.assets.Resource;
import com.example.testbot.assets.VillageResources;
import com.example.testbot.helpers.Converter;

public class PopupResourcesView extends ResourcesView {
    private TextView mTvTimeLeftLumber;
    private TextView mTvTimeLeftClay;
    private TextView mTvTimeLeftIron;
    private TextView mTvTimeLeftCrop;

    private TextView mTvStorageLumber;
    private TextView mTvStorageClay;
    private TextView mTvStorageIron;
    private TextView mTvStorageCrop;

    private TextView mTvProductionLumber;
    private TextView mTvProductionClay;
    private TextView mTvProductionIron;
    private TextView mTvProductionCrop;

    private TextView mTvCapacityWarehouse;
    private TextView mTVCapacityGranary;

    public PopupResourcesView(Activity activity) {
        View lumberHolder = activity.findViewById(R.id.lumber);
        View clayHolder = activity.findViewById(R.id.clay);
        View ironHolder = activity.findViewById(R.id.iron);
        View cropHolder = activity.findViewById(R.id.crop);

        mPbLumber   = lumberHolder.findViewById(R.id.progressbar);
        mPbClay     = clayHolder.findViewById(R.id.progressbar);
        mPbIron     = ironHolder.findViewById(R.id.progressbar);
        mPbCrop     = cropHolder.findViewById(R.id.progressbar);

        mTvTimeLeftLumber  = lumberHolder.findViewById(R.id.tv_time_left);
        mTvTimeLeftClay    = clayHolder.findViewById(R.id.tv_time_left);
        mTvTimeLeftIron    = ironHolder.findViewById(R.id.tv_time_left);
        mTvTimeLeftCrop    = cropHolder.findViewById(R.id.tv_time_left);

        mTvStorageLumber   = lumberHolder.findViewById(R.id.tv_storage);
        mTvStorageClay     = clayHolder.findViewById(R.id.tv_storage);
        mTvStorageIron     = ironHolder.findViewById(R.id.tv_storage);
        mTvStorageCrop     = cropHolder.findViewById(R.id.tv_storage);

        mTvProductionLumber  = lumberHolder.findViewById(R.id.tv_production);
        mTvProductionClay    = clayHolder.findViewById(R.id.tv_production);
        mTvProductionIron    = ironHolder.findViewById(R.id.tv_production);
        mTvProductionCrop    = cropHolder.findViewById(R.id.tv_production);

        mTVCapacityGranary   = activity.findViewById(R.id.granary).findViewById(R.id.capacity);
        mTvCapacityWarehouse = activity.findViewById(R.id.warehouse).findViewById(R.id.capacity);

        setAnimationDuration(1200);
    }

    @Override
    public void updateResources(VillageResources village) {
        // refresh all progress bars
        super.updateResources(village);

        // refresh time left until full
        updateTimeLeft(mTvTimeLeftLumber, village.lumber);
        updateTimeLeft(mTvTimeLeftClay,   village.clay);
        updateTimeLeft(mTvTimeLeftIron,   village.iron);
        updateTimeLeft(mTvTimeLeftCrop,   village.crop);

        // refresh current storage
        updateStorage(mTvStorageLumber, village.lumber);
        updateStorage(mTvStorageClay,   village.clay);
        updateStorage(mTvStorageIron,   village.iron);
        updateStorage(mTvStorageCrop,   village.crop);

        // refresh current productions
        updateProduction(mTvProductionLumber, village.lumber);
        updateProduction(mTvProductionClay, village.clay);
        updateProduction(mTvProductionIron, village.iron);
        updateProduction(mTvProductionCrop, village.crop);

        //refresh current capacity
        updateCapacity(mTvCapacityWarehouse, village.lumber);
        updateCapacity(mTVCapacityGranary,   village.crop);
    }

    public void timerUpdate(VillageResources village) {
        // refresh current storage
        updateStorage(mTvStorageLumber, village.lumber);
        updateStorage(mTvStorageClay,   village.clay);
        updateStorage(mTvStorageIron,   village.iron);
        updateStorage(mTvStorageCrop,   village.crop);

        // refresh time left until full
        updateTimeLeft(mTvTimeLeftLumber, village.lumber);
        updateTimeLeft(mTvTimeLeftClay,   village.clay);
        updateTimeLeft(mTvTimeLeftIron,   village.iron);
        updateTimeLeft(mTvTimeLeftCrop,   village.crop);
    }

    private void updateStorage(TextView tvStorage, Resource resource) {
//        tvStorage.setText(String.valueOf((int) resource.storage));
        tvStorage.setText(String.valueOf((int) resource.getStorage()));
    }

    private void updateCapacity(TextView tvCapacity, Resource resource) {
        tvCapacity.setText(String.valueOf(resource.capacity));
    }

    private void updateProduction(TextView tvProduction, Resource resource) {
        tvProduction.setText(String.valueOf(resource.productionPerHour));
    }

    /*
        Just formatting time, not trying to change any texts, so there will not be any DefaultLocale bugs
     */
    @SuppressLint("DefaultLocale")
    private void updateTimeLeft(TextView tvTimeLeft, Resource resource) {
        int timeLeft = resource.getTimeLeftSecond();

        tvTimeLeft.setText(Converter.secToString(timeLeft));
    }
}
